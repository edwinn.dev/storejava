package com.ccf.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public final class Http {
    
    public static String httpGetDniClient(String dni) throws Exception {
        String urlApi = "https://api.apis.net.pe/v1/dni?numero=".concat(dni.trim());
        StringBuilder response = new StringBuilder();
        URL url = new URL(urlApi);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        try (BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
        }
        return response.toString();
    }
}
