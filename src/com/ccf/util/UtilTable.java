package com.ccf.util;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class UtilTable {
    
    public static void clearTableModel(JTable table){
        DefaultTableModel model = (DefaultTableModel)table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
}
