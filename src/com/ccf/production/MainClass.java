package com.ccf.production;

import com.ccf.controller.ControllerLogin;
import com.ccf.view.FormLogin;
import com.formdev.flatlaf.intellijthemes.FlatCyanLightIJTheme;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MainClass {
    public static void main(String args[]) {
        try { 
            UIManager.setLookAndFeel ( new FlatCyanLightIJTheme()); 
        } catch (UnsupportedLookAndFeelException ex) { 
            System.err.println( "ERROR INIT UI THEME" ); 
        }
        
        java.awt.EventQueue.invokeLater(() -> {
            FormLogin form = FormLogin.getInstance();
            ControllerLogin controllerLogin = new ControllerLogin(form);
            controllerLogin.showLogin();
        });
    }
}
