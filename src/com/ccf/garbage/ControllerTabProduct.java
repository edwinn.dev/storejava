package com.ccf.garbage;

import com.ccf.controller.ControllerProduct;
import com.ccf.garbage.FormMain;
import com.ccf.view.FormProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControllerTabProduct implements ActionListener, MouseListener{
    private FormMain formMain = null;
    
    public ControllerTabProduct (FormMain formMain){
        this.formMain = formMain;
        setListener();
    }
    
    private void setListener(){
        this.formMain.btnNuevoProducto.addActionListener(this);
        this.formMain.tableProducts.addMouseListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == formMain.btnNuevoProducto){
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
