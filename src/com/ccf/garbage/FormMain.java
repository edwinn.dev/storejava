package com.ccf.garbage;

import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class FormMain extends javax.swing.JFrame {
    private ImageIcon IC_B_HOME;
    private ImageIcon IC_B_INVE;
    private ImageIcon IC_B_SALE;
    private ImageIcon IC_B_SHOP;
    private ImageIcon IC_B_REPT;
    private ImageIcon IC_B_USER;
    private static FormMain instance = null;
    private int widthPanel = 0;
    private boolean show = true;
    
    private FormMain() {
        initComponents();
        //setPreferences();
    }

    public synchronized static FormMain getInstance(){
        if(instance == null){
            instance = new FormMain();
        }
        return instance;
    }
    
    private void setPreferences(){
        this.btnMenu.setSize(40, 30);
        this.btnMenu.setOpaque(false);
        this.btnMenu.setBorderPainted(false);
        this.btnMenu.setContentAreaFilled(false);
        widthPanel = panelMenu.getWidth();
        this.setIconImage(new ImageIcon(getClass().getResource("/com/ccf/images/icon_login1.png")).getImage());
        ImageIcon iconLogin = new ImageIcon(getClass().getResource("/com/ccf/images/icon_menu2.png"));
        Icon fondo = new ImageIcon(iconLogin.getImage().getScaledInstance(btnMenu.getWidth() - 5, btnMenu.getHeight(), Image.SCALE_DEFAULT));
        this.btnMenu.setIcon(fondo);
        
        IC_B_HOME = new ImageIcon(getClass().getResource("/com/ccf/images/icon_home.png"));
        IC_B_INVE = new ImageIcon(getClass().getResource("/com/ccf/images/icon_inve.png"));
        IC_B_SALE = new ImageIcon(getClass().getResource("/com/ccf/images/icon_sale.png"));
        IC_B_SHOP = new ImageIcon(getClass().getResource("/com/ccf/images/icon_shop.png"));
        IC_B_REPT = new ImageIcon(getClass().getResource("/com/ccf/images/icon_rept.png"));
        IC_B_USER = new ImageIcon(getClass().getResource("/com/ccf/images/icon_users.png"));
        
        this.btnHome.setIcon(new ImageIcon(IC_B_HOME.getImage().getScaledInstance(20, btnHome.getHeight() - 8, Image.SCALE_DEFAULT)));
        this.btnInve.setIcon(new ImageIcon(IC_B_INVE.getImage().getScaledInstance(20, btnInve.getHeight() - 8, Image.SCALE_DEFAULT)));
        this.btnSale.setIcon(new ImageIcon(IC_B_SALE.getImage().getScaledInstance(20, btnSale.getHeight() - 8, Image.SCALE_DEFAULT)));
        this.btnShop.setIcon(new ImageIcon(IC_B_SHOP.getImage().getScaledInstance(20, btnShop.getHeight() - 8, Image.SCALE_DEFAULT)));
        this.btnRept.setIcon(new ImageIcon(IC_B_REPT.getImage().getScaledInstance(20, btnRept.getHeight() - 8, Image.SCALE_DEFAULT)));
        this.btnUsers.setIcon(new ImageIcon(IC_B_USER.getImage().getScaledInstance(20, btnUsers.getHeight() - 8, Image.SCALE_DEFAULT)));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane8 = new javax.swing.JTabbedPane();
        layeredHome = new javax.swing.JLayeredPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        layeredProducts = new javax.swing.JLayeredPane();
        jPanel2 = new javax.swing.JPanel();
        btnNuevoProducto = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProducts = new javax.swing.JTable();
        layeredInventary = new javax.swing.JLayeredPane();
        layeredClients = new javax.swing.JLayeredPane();
        layeredEmployees = new javax.swing.JLayeredPane();
        layeredVoucher = new javax.swing.JLayeredPane();
        panelMenu = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnHome = new javax.swing.JButton();
        btnInve = new javax.swing.JButton();
        btnSale = new javax.swing.JButton();
        btnShop = new javax.swing.JButton();
        btnRept = new javax.swing.JButton();
        btnUsers = new javax.swing.JButton();
        btnMenu = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 122, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 206, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("HOME", jLayeredPane1);
        jTabbedPane1.addTab("INVENTARIO", jTabbedPane2);

        layeredHome.setLayer(jTabbedPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layeredHomeLayout = new javax.swing.GroupLayout(layeredHome);
        layeredHome.setLayout(layeredHomeLayout);
        layeredHomeLayout.setHorizontalGroup(
            layeredHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredHomeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(496, Short.MAX_VALUE))
        );
        layeredHomeLayout.setVerticalGroup(
            layeredHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredHomeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(248, Short.MAX_VALUE))
        );

        jTabbedPane8.addTab("HOME", layeredHome);

        btnNuevoProducto.setText("NUEVO");

        tableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE DEL PRODUCTO", "STOCK", "PRECIO COMPRA", "PRECIO VENTA", "FECHA DE REGISTRO", "FECHA VENCIMIENTO", "ESTADO", "UNIDAD MEDIDA"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableProducts);
        if (tableProducts.getColumnModel().getColumnCount() > 0) {
            tableProducts.getColumnModel().getColumn(0).setResizable(false);
            tableProducts.getColumnModel().getColumn(0).setPreferredWidth(5);
            tableProducts.getColumnModel().getColumn(1).setResizable(false);
            tableProducts.getColumnModel().getColumn(1).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(2).setResizable(false);
            tableProducts.getColumnModel().getColumn(2).setPreferredWidth(10);
            tableProducts.getColumnModel().getColumn(3).setResizable(false);
            tableProducts.getColumnModel().getColumn(3).setPreferredWidth(10);
            tableProducts.getColumnModel().getColumn(4).setResizable(false);
            tableProducts.getColumnModel().getColumn(4).setPreferredWidth(10);
            tableProducts.getColumnModel().getColumn(5).setResizable(false);
            tableProducts.getColumnModel().getColumn(5).setPreferredWidth(50);
            tableProducts.getColumnModel().getColumn(6).setResizable(false);
            tableProducts.getColumnModel().getColumn(6).setPreferredWidth(50);
            tableProducts.getColumnModel().getColumn(7).setResizable(false);
            tableProducts.getColumnModel().getColumn(7).setPreferredWidth(20);
            tableProducts.getColumnModel().getColumn(8).setResizable(false);
            tableProducts.getColumnModel().getColumn(8).setPreferredWidth(50);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnNuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 667, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 191, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        layeredProducts.setLayer(jPanel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layeredProductsLayout = new javax.swing.GroupLayout(layeredProducts);
        layeredProducts.setLayout(layeredProductsLayout);
        layeredProductsLayout.setHorizontalGroup(
            layeredProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredProductsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layeredProductsLayout.setVerticalGroup(
            layeredProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredProductsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane8.addTab("PRODUCTOS", layeredProducts);

        javax.swing.GroupLayout layeredInventaryLayout = new javax.swing.GroupLayout(layeredInventary);
        layeredInventary.setLayout(layeredInventaryLayout);
        layeredInventaryLayout.setHorizontalGroup(
            layeredInventaryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 707, Short.MAX_VALUE)
        );
        layeredInventaryLayout.setVerticalGroup(
            layeredInventaryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
        );

        jTabbedPane8.addTab("INVENTARIO", layeredInventary);

        javax.swing.GroupLayout layeredClientsLayout = new javax.swing.GroupLayout(layeredClients);
        layeredClients.setLayout(layeredClientsLayout);
        layeredClientsLayout.setHorizontalGroup(
            layeredClientsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 707, Short.MAX_VALUE)
        );
        layeredClientsLayout.setVerticalGroup(
            layeredClientsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
        );

        jTabbedPane8.addTab("CLIENTES", layeredClients);

        javax.swing.GroupLayout layeredEmployeesLayout = new javax.swing.GroupLayout(layeredEmployees);
        layeredEmployees.setLayout(layeredEmployeesLayout);
        layeredEmployeesLayout.setHorizontalGroup(
            layeredEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 707, Short.MAX_VALUE)
        );
        layeredEmployeesLayout.setVerticalGroup(
            layeredEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
        );

        jTabbedPane8.addTab("TRABAJADORES", layeredEmployees);

        javax.swing.GroupLayout layeredVoucherLayout = new javax.swing.GroupLayout(layeredVoucher);
        layeredVoucher.setLayout(layeredVoucherLayout);
        layeredVoucherLayout.setHorizontalGroup(
            layeredVoucherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 707, Short.MAX_VALUE)
        );
        layeredVoucherLayout.setVerticalGroup(
            layeredVoucherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
        );

        jTabbedPane8.addTab("COMPROBANTES", layeredVoucher);

        panelMenu.setBackground(new java.awt.Color(102, 102, 102));
        panelMenu.setMaximumSize(new java.awt.Dimension(150, 32767));
        panelMenu.setPreferredSize(new java.awt.Dimension(150, 428));

        jLabel1.setText("USER IMAGE");

        jLabel2.setText("USER NAME");

        btnHome.setText("INICIO");
        btnHome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnInve.setText("INVENTARIO");
        btnInve.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnSale.setText("VENTAS");
        btnSale.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnShop.setText("COMPRAS");
        btnShop.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnRept.setText("REPORTES");
        btnRept.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnUsers.setText("USUARIOS");
        btnUsers.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnMenu.setMaximumSize(new java.awt.Dimension(30, 10));
        btnMenu.setMinimumSize(new java.awt.Dimension(0, 0));
        btnMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMenuLayout.createSequentialGroup()
                        .addGroup(panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnInve, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                            .addComponent(btnHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSale, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnShop, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRept, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUsers, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(15, 15, 15))
                    .addGroup(panelMenuLayout.createSequentialGroup()
                        .addGroup(panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panelMenuLayout.createSequentialGroup()
                                .addComponent(btnMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnInve, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSale, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnShop, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRept, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane8)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane8)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuActionPerformed
        if(show){
            panelMenu.setSize(btnMenu.getWidth() + 30, panelMenu.getHeight());
            show = false;
        }else{
            panelMenu.setSize(widthPanel, panelMenu.getHeight());
            show = true;
        }
    }//GEN-LAST:event_btnMenuActionPerformed

    public static void main(String[] args) {
        new FormMain().setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHome;
    private javax.swing.JButton btnInve;
    private javax.swing.JButton btnMenu;
    public javax.swing.JButton btnNuevoProducto;
    private javax.swing.JButton btnRept;
    private javax.swing.JButton btnSale;
    private javax.swing.JButton btnShop;
    private javax.swing.JButton btnUsers;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane8;
    private javax.swing.JLayeredPane layeredClients;
    private javax.swing.JLayeredPane layeredEmployees;
    private javax.swing.JLayeredPane layeredHome;
    private javax.swing.JLayeredPane layeredInventary;
    private javax.swing.JLayeredPane layeredProducts;
    private javax.swing.JLayeredPane layeredVoucher;
    private javax.swing.JPanel panelMenu;
    public javax.swing.JTable tableProducts;
    // End of variables declaration//GEN-END:variables
}
