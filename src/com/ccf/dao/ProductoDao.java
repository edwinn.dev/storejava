package com.ccf.dao;

import com.ccf.model.Product;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.ccf.database.ConnectionDb.getConnectionInstance;
import com.ccf.database.Query;

public class ProductoDao implements Repository<Product>{
    
    @Override
    public List<Product> findAll() {
        List<Product> productos = new ArrayList<>();
        try (PreparedStatement ps =  getConnectionInstance().prepareStatement(Query.SQL_PRODUCT_FIND_ALL);
             ResultSet rs = ps.executeQuery()) {
            while(rs.next()){
                Product prod = new Product();
                prod.setIdProducto(rs.getLong("idProducto"));
                prod.setCodProducto(rs.getString("codProducto"));
                prod.setIdCategoria(rs.getInt("idCategoria"));
                prod.setIdUnidadMedida(rs.getString("idUnidadMedida"));
                prod.setNombreProducto(rs.getString("nombre"));
                prod.setPrecioVenta(rs.getDouble("precioVenta"));
                prod.setPrecioCompra(rs.getDouble("precioCompra"));
                prod.setStock(rs.getDouble("stock"));
                prod.setDescripcion(rs.getString("descripcion"));
                prod.setNombreCategoria(rs.getString("categoria"));
                prod.setNombreUnMedida(rs.getString("unidadMedida"));
                productos.add(prod);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return productos;
    }

    @Override
    public List<Product> findAllWhere(Object condition, Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Product findById(Product t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int create(Product t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Product t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Product t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
