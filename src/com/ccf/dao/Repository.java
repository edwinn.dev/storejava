package com.ccf.dao;

import java.util.List;

public interface Repository<T> {
    List<T> findAll();
    List<T> findAllWhere(Object condition, Object value);
    T findById(T t);
    int create(T t);
    int update(T t);
    int delete(T t);
}
