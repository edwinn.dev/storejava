package com.ccf.dao;

import com.ccf.database.Query;
import com.ccf.model.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.ccf.database.ConnectionDb.getConnectionInstance;

public class UsuarioDao {
    
    public Usuario findById(Usuario user){
        Usuario userReturn = null;
        try (PreparedStatement ps = getConnectionInstance().prepareStatement("")){
            ps.setInt(1, user.getIdUsuario());
            try (ResultSet rs = ps.executeQuery()) {
                if(rs.next()){
                    userReturn = new Usuario();
                    userReturn.setIdUsuario(rs.getInt("user_id"));
                    userReturn.setUserName(rs.getString("user_name"));
                    userReturn.setPassword(rs.getString("user_password"));
                    userReturn.setIdRol(rs.getInt("user_role"));
                    userReturn.setUserState(rs.getString("user_state"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
            userReturn = null;
        }
        return userReturn;
    }
    
    public Usuario loginUser(Usuario user){
        Usuario userReturn = null;
        try (PreparedStatement ps = getConnectionInstance().prepareStatement(Query.SQL_SIGN_IN)){
            ps.setInt(1, user.getIdUsuario());
            try (ResultSet rs = ps.executeQuery()) {
                if(rs.next()){
                    userReturn = new Usuario();
                    userReturn.setIdUsuario(rs.getInt("user_id"));
                    userReturn.setUserName(rs.getString("user_name"));
                    userReturn.setPassword(rs.getString("user_password"));
                    userReturn.setIdRol(rs.getInt("user_role"));
                    userReturn.setUserState(rs.getString("user_state"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
            userReturn = null;
        }
        return userReturn;
    }
}
