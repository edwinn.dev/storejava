package com.ccf.dao;

import static com.ccf.database.ConnectionDb.getConnectionInstance;
import com.ccf.database.Query;
import com.ccf.model.Proveedor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProveedorDao implements Repository<Proveedor>{

    @Override
    public List<Proveedor> findAll() {
        List<Proveedor> proveedores = new ArrayList<>();
        try (PreparedStatement ps =  getConnectionInstance().prepareStatement(Query.SQL_PROV_FIND_ALL);
             ResultSet rs = ps.executeQuery()) {
            while(rs.next()){
                Proveedor proveedor = new Proveedor();
                proveedor.setIdProveedor(rs.getLong("idProveedor"));
                proveedor.setRazonSocial(rs.getString("razonSocial"));
                proveedor.setRuc(rs.getString("ruc"));
                proveedor.setDireccion(rs.getString("direccion"));
                proveedor.setTelefono(rs.getString("telefono"));
                proveedor.setEmail(rs.getString("email"));
                proveedores.add(proveedor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proveedores;
    }

    @Override
    public List<Proveedor> findAllWhere(Object condition, Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Proveedor findById(Proveedor p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int create(Proveedor p) {
        int response;
        try (PreparedStatement ps =  getConnectionInstance().prepareStatement(Query.SQL_PROV_CREATE)) {
            ps.setString(1, p.getRazonSocial());
            ps.setString(2, p.getRuc());
            ps.setString(3, p.getDireccion());
            ps.setString(4, p.getTelefono());
            ps.setString(5, p.getEmail());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorDao.class.getName()).log(Level.SEVERE, null, ex);
            response = -1;
        }
        return response;
    }

    @Override
    public int update(Proveedor p) {
        int response;
        try (PreparedStatement ps =  getConnectionInstance().prepareStatement(Query.SQL_PROV_UPDATE)) {
            ps.setString(1, p.getRazonSocial());
            ps.setString(2, p.getRuc());
            ps.setString(3, p.getDireccion());
            ps.setString(4, p.getTelefono());
            ps.setString(5, p.getEmail());
            ps.setLong(6, p.getIdProveedor());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorDao.class.getName()).log(Level.SEVERE, null, ex);
            response = -1;
        }
        return response;
    }

    @Override
    public int delete(Proveedor p) {
        int response;
        try (PreparedStatement ps =  getConnectionInstance().prepareStatement(Query.SQL_PROV_DELETE)) {
            ps.setLong(1, p.getIdProveedor());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorDao.class.getName()).log(Level.SEVERE, null, ex);
            response = -1;
        }
        return response;
    }
}
