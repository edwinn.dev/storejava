package com.ccf.view;

import com.ccf.util.TextPrompt;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class FormLogin extends javax.swing.JFrame {
    private static FormLogin instance = null;
    
    private FormLogin() {
        initComponents();
        setPreferences();
    }
    
    public static FormLogin getInstance(){
        if(instance == null){
            instance = new FormLogin();
        }
        return instance;
    }
    
    private void setPreferences(){
        this.setIconImage(new ImageIcon(getClass().getResource("/com/ccf/images/icon_login1.png")).getImage());
        
        ImageIcon iconLogin = new ImageIcon(getClass().getResource("/com/ccf/images/icon_login1.png"));
        Icon fondo = new ImageIcon(iconLogin.getImage().getScaledInstance(lblIcon.getWidth(), lblIcon.getHeight(), Image.SCALE_DEFAULT));
        this.lblIcon.setIcon(fondo);
        
        ImageIcon iconAltern = new ImageIcon(getClass().getResource("/com/ccf/images/icon_credit_card.png"));
        Icon fondo2 = new ImageIcon(iconAltern.getImage().getScaledInstance(lblIconAltern.getWidth(), lblIconAltern.getHeight(), Image.SCALE_DEFAULT));
        this.lblIconAltern.setIcon(fondo2);
        
        TextPrompt placeHolder = new TextPrompt("usuario", this.txtUserName);
        placeHolder.changeAlpha(0.6f);
        TextPrompt placeHolder2 = new TextPrompt("password", this.txtPasswordHidden);
        placeHolder2.changeAlpha(0.6f);
        TextPrompt placeHolder3 = new TextPrompt("password", this.txtPasswordView);
        placeHolder3.changeAlpha(0.6f);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIcon = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtUserName = new javax.swing.JFormattedTextField();
        txtPasswordHidden = new javax.swing.JPasswordField();
        txtPasswordView = new javax.swing.JFormattedTextField();
        chkMostrar = new javax.swing.JCheckBox();
        btnIngresar = new javax.swing.JButton();
        lblIconAltern = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("BODEGA store.com ccf");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 228, 90));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("User Name");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 116, 69, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Password");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 69, 30));

        txtUserName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtUserName.setMargin(new java.awt.Insets(5, 5, 5, 5));
        jPanel1.add(txtUserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(89, 117, 140, 30));

        txtPasswordHidden.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPasswordHidden.setMargin(new java.awt.Insets(5, 5, 5, 5));
        jPanel1.add(txtPasswordHidden, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 140, 30));

        txtPasswordView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPasswordView.setMargin(new java.awt.Insets(5, 5, 5, 5));
        jPanel1.add(txtPasswordView, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 140, 30));

        chkMostrar.setText("Mostrar contraseña");
        chkMostrar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(chkMostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 220, -1));

        btnIngresar.setBackground(new java.awt.Color(51, 204, 255));
        btnIngresar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnIngresar.setText("Sign In");
        btnIngresar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 310, 110, 40));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(85, 85, 85))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblIconAltern, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblIconAltern, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnIngresar;
    public javax.swing.JCheckBox chkMostrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblIcon;
    private javax.swing.JLabel lblIconAltern;
    public javax.swing.JPasswordField txtPasswordHidden;
    public javax.swing.JFormattedTextField txtPasswordView;
    public javax.swing.JFormattedTextField txtUserName;
    // End of variables declaration//GEN-END:variables
}
