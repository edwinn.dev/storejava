package com.ccf.view.menu;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTablaProveedor {
    private static MenuTablaProveedor instance = null;
    private JPopupMenu menu;
    public JMenuItem itemUpdateStock;
    public JMenuItem itemUpdateProduct;
    public JMenuItem itemDeleteProduct;
    
    private MenuTablaProveedor(){}
    
    public static MenuTablaProveedor getInstance(){
        if(instance == null){
            instance = new MenuTablaProveedor();
        }
        return instance;
    }
    
    public void initMenuProduct(JTable table){
        menu = new JPopupMenu();
        itemUpdateStock = new JMenuItem("Actualizar PROVEEDOR");
        itemUpdateProduct = new JMenuItem("Modificar producto");
        itemDeleteProduct = new JMenuItem("Eliminar producto");
        
        menu.add(itemUpdateStock);
        menu.add(itemUpdateProduct);
        menu.add(itemDeleteProduct);
        table.setComponentPopupMenu(menu);
    }
}
