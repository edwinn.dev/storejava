package com.ccf.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDb {
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "edwinserver";
    private static final String URL = "jdbc:mysql://localhost:3306/bodega_victorio";
    private static Connection instance = null;
    
    private ConnectionDb(){}
    
    public static synchronized Connection getConnectionInstance() throws SQLException{
        if(instance == null){
            instance = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        }
        return instance;
    }
    
    public static synchronized void closeConnection(){
        try {
            instance.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDb.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("ERROR AL CERRAR LA CONECCION : " + ConnectionDb.class.getName() + ", LINEA 29");
        }
    }
}
