package com.ccf.database;

public interface Query {
    //USUARIOS
    String SQL_SIGN_IN = "";
    
    //PRODUCTOS
    String SQL_PRODUCT_FIND_ALL = "SELECT p.*, cat.nombre as categoria, um.nombre as unidadMedida FROM PRODUCTO p INNER JOIN CATEGORIA cat ON p.idCategoria = cat.idCategoria INNER JOIN UNIDAD_MEDIDA um ON p.idUnidadMedida = um.idUnidadMedida";

    //PROVEEDORES
    String SQL_PROV_FIND_ALL = "SELECT * FROM PROVEEDOR";
    String SQL_PROV_CREATE = "INSERT INTO PROVEEDOR (razonSocial, ruc, direccion, telefono, email) VALUES (?,?,?,?,?)";
    String SQL_PROV_UPDATE = "UPDATE PROVEEDOR SET razonSocial=?, ruc=?, direccion=?, telefono=?, email=? WHERE idProveedor=?";
    String SQL_PROV_DELETE = "DELETE FROM PROVEEDOR WHERE idProveedor=?";
}
