package com.ccf.model;

public class Usuario {
    private Integer idUsuario;
    private Integer idRol;
    private String userName;
    private String user;
    private String password;
    private String userState;
    
    public Usuario(){}

    public Usuario(Integer idUsuario, Integer idRol, String userName, String user, String password, String userState) {
        this.idUsuario = idUsuario;
        this.idRol = idRol;
        this.userName = userName;
        this.user = user;
        this.password = password;
        this.userState = userState;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Usuario{idUsuario=").append(idUsuario);
        sb.append(", idRol=").append(idRol);
        sb.append(", userName=").append(userName);
        sb.append(", user=").append(user);
        sb.append(", password=").append(password);
        sb.append(", userState=").append(userState);
        sb.append('}');
        return sb.toString();
    }
}
