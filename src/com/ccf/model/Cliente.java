package com.ccf.model;

public class Cliente {
    private Integer idCliente;
    private String nroDocumento;
    private String nombre;
    private String ultimaCompra;
    private Long nroCompras;

    public Cliente() {
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUltimaCompra() {
        return ultimaCompra;
    }

    public void setUltimaCompra(String ultimaCompra) {
        this.ultimaCompra = ultimaCompra;
    }

    public Long getNroCompras() {
        return nroCompras;
    }

    public void setNroCompras(Long nroCompras) {
        this.nroCompras = nroCompras;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cliente{idCliente=").append(idCliente);
        sb.append(", nroDocumento=").append(nroDocumento);
        sb.append(", nombre=").append(nombre);
        sb.append(", ultimaCompra=").append(ultimaCompra);
        sb.append(", nroCompras=").append(nroCompras);
        sb.append('}');
        return sb.toString();
    }
}
