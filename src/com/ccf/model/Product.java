package com.ccf.model;

public class Product {
    private Long idProducto;
    private String codProducto;
    private Integer idCategoria;
    private String idUnidadMedida;
    private String nombreProducto;
    private Double precioCompra;
    private Double precioVenta;
    private Double stock;
    private String descripcion;
    private String nombreUnMedida;
    private String nombreCategoria;
    
    private Double auxQuantity;
    private Double auxPrecioUnitario;
    private Double auxSubtotal;
    
    public Product() { }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdUnidadMedida() {
        return idUnidadMedida;
    }

    public void setIdUnidadMedida(String idUnidadMedida) {
        this.idUnidadMedida = idUnidadMedida;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(Double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public Double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreUnMedida() {
        return nombreUnMedida;
    }

    public void setNombreUnMedida(String nombreUnMedida) {
        this.nombreUnMedida = nombreUnMedida;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }
    
    public Double getAuxQuantity() {
        return auxQuantity;
    }

    public void setAuxQuantity(Double auxQuantity) {
        this.auxQuantity = auxQuantity;
    }
    
    public Double getAuxPrecioUnitario() {
        return auxPrecioUnitario;
    }

    public void setAuxPrecioUnitario(Double auxPrecioUnitario) {
        this.auxPrecioUnitario = auxPrecioUnitario;
    }

    public Double getAuxSubtotal() {
        return auxSubtotal;
    }

    public void setAuxSubtotal(Double auxSubtotal) {
        this.auxSubtotal = auxSubtotal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Product{idProducto=").append(idProducto);
        sb.append(", codProducto=").append(codProducto);
        sb.append(", idCategoria=").append(idCategoria);
        sb.append(", idUnidadMedida=").append(idUnidadMedida);
        sb.append(", nombreProducto=").append(nombreProducto);
        sb.append(", precioCompra=").append(precioCompra);
        sb.append(", precioVenta=").append(precioVenta);
        sb.append(", stock=").append(stock);
        sb.append(", descripcion=").append(descripcion);
        sb.append(", nombreUnMedida=").append(nombreUnMedida);
        sb.append(", nombreCategoria=").append(nombreCategoria);
        sb.append('}');
        return sb.toString();
    }
}
