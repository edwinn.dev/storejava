package com.ccf.model;

public class Proveedor {
    private Long idProveedor;
    private String razonSocial;
    private String ruc;
    private String direccion;
    private String Telefono;
    private String email;

    public Proveedor() {
    }
    
    public Proveedor(Long idProveedor){
        this.idProveedor = idProveedor;
    }

    public Proveedor(Long idProveedor, String razonSocial, String ruc, String direccion, String Telefono, String email) {
        this.idProveedor = idProveedor;
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.direccion = direccion;
        this.Telefono = Telefono;
        this.email = email;
    }

    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Proveedor{idProveedor=").append(idProveedor);
        sb.append(", razonSocial=").append(razonSocial);
        sb.append(", ruc=").append(ruc);
        sb.append(", direccion=").append(direccion);
        sb.append(", Telefono=").append(Telefono);
        sb.append(", email=").append(email);
        sb.append('}');
        return sb.toString();
    }
}
