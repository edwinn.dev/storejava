package com.ccf.controller.pref;

import com.ccf.view.FormHome;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ControllerPreferences implements ActionListener{
    private static final String PATH_THEME = "res\\theme.txt";
    private FormHome mainForm = null;
    
    public ControllerPreferences(FormHome mainForm){
        this.mainForm = mainForm;
    }
    
    public void initViewMenu(){
        this.mainForm.themeDark.addActionListener(this);
        this.mainForm.themeLigth.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == mainForm.themeDark){
            writeFilePreferences(PATH_THEME, "1");
            System.out.println("THEME themeDark");
        }else if(e.getSource() == mainForm.themeLigth){
            writeFilePreferences(PATH_THEME, "2");
            System.out.println("THEME themeLigth");
        }
        
        JOptionPane.showMessageDialog(mainForm, "ES NECESARIO REINICAR EL PROGRAMA\nPARA VISUALIZAR LOS CAMBIOS");
    }
    
    private void writeFilePreferences(String fileName, String content){
        File file = new File(fileName);
        try {
            try (PrintWriter writer = new PrintWriter(file)) {
                writer.print(content.trim());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControllerPreferences.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("ERROR AL CREAR ARCHIVO");
        }
    }
}
