package com.ccf.controller;

import com.ccf.view.FormHome;
import com.ccf.view.FormProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerProduct implements ActionListener{
    private FormProducto formProduct = null;
    private FormHome mainForm = null;
    
    public ControllerProduct(FormProducto formProduct, FormHome mainForm){
        this.formProduct = formProduct;
        this.mainForm = mainForm;
    }
    
    public void initViewNewProduct(){
        this.formProduct.btnRegistrar.addActionListener(this);
        this.formProduct.btnActualizar.addActionListener(this);
        this.formProduct.setModal(true);
        this.formProduct.setResizable(false);
        this.formProduct.setLocationRelativeTo(mainForm);
        this.formProduct.setVisible(true);
    }
    
    public void showFormProduct(){
        this.formProduct.setLocationRelativeTo(null);
        this.formProduct.setVisible(true);
        this.formProduct.setResizable(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == formProduct.btnRegistrar){
            
        }
    }
}
