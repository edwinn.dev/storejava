package com.ccf.controller;

import com.ccf.view.FormHome;
import com.ccf.view.card.Card;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerTabHome implements ActionListener {
    private FormHome mainForm = null;
    
    public ControllerTabHome(FormHome mainForm){
        this.mainForm = mainForm;
    }
    
    public void initCardsView(){
        Card card1 = new Card();
        card1.setTitle("PROVEEDORES");
        card1.setQuantity("5");
        card1.setColor(new Color(0xB97A95));
        card1.setImage("src/com/ccf/images/card_supplier.png");
        
        Card card2 = new Card();
        card2.setTitle("VENTAS DEL DIA");
        card2.setQuantity("$ 500");
        card2.setColor(new Color(0xFFB319));
        card2.setImage("src/com/ccf/images/card_sale.png");
        
        Card card3 = new Card();
        card3.setTitle("PRODUCTOS CON STOCK");
        card3.setQuantity("100");
        card3.setColor(new Color(0x8FC1D4));
        card3.setImage("src/com/ccf/images/card_products.png");
        
        Card card4 = new Card();
        card4.setTitle("CATEGORIAS DE PRODUCTOS");
        card4.setQuantity("14");
        card4.setColor(new Color(0xF43B86));
        card4.setImage("src/com/ccf/images/card_categories.png");
        
        Card card5 = new Card();
        card5.setTitle("CLIENTES DEL DIA");
        card5.setQuantity("56");
        card5.setColor(new Color(0x628395));
        card5.setImage("src/com/ccf/images/card_clients.png");
        
        Card card6 = new Card();
        card6.setTitle("TRABAJADORES");
        card6.setQuantity("3");
        card6.setColor(new Color(0xE99497));
        card6.setImage("src/com/ccf/images/card_users.png");
        
        this.mainForm.panelCardHome.add(card1);
        this.mainForm.panelCardHome.add(card2);
        this.mainForm.panelCardHome.add(card3);
        this.mainForm.panelCardHome.add(card4);
        this.mainForm.panelCardHome.add(card5);
        this.mainForm.panelCardHome.add(card6);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}
