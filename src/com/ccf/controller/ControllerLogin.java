package com.ccf.controller;

import com.ccf.controller.pref.ControllerPreferences;
import com.ccf.view.FormHome;
import com.ccf.view.FormLogin;
import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.intellijthemes.FlatCyanLightIJTheme;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ControllerLogin implements ActionListener {

    private static final String PATH_THEME = "res\\theme.txt";
    private final FormLogin formLogin;

    public ControllerLogin(FormLogin formLogin) {
        this.formLogin = formLogin;
        setListener();
    }

    private void setListener() {
        formLogin.chkMostrar.addActionListener(this);
        formLogin.btnIngresar.addActionListener(this);
    }

    public void showLogin() {
        formLogin.setLocationRelativeTo(null);
        formLogin.setResizable(false);
        formLogin.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == formLogin.chkMostrar) {
            if (formLogin.chkMostrar.isSelected()) {
                formLogin.txtPasswordView.setVisible(true);
                formLogin.txtPasswordHidden.setVisible(false);
                formLogin.txtPasswordView.setText(formLogin.txtPasswordHidden.getText());
            } else {
                formLogin.txtPasswordView.setVisible(false);
                formLogin.txtPasswordHidden.setVisible(true);
                formLogin.txtPasswordHidden.setText(formLogin.txtPasswordView.getText());
            }
            return;
        }

        if (e.getSource() == formLogin.btnIngresar) {
            this.formLogin.dispose();
            
            String value = getTheme(PATH_THEME);
            if (value == null) {
                try {
                    UIManager.setLookAndFeel(new FlatCyanLightIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                    System.err.println("ERROR AL INICIALIZAR UI_THEME");
                }
            }else{
                switch (value.trim()) {
                    case "1":
                        try {
                            UIManager.setLookAndFeel(new FlatDarculaLaf());
                        } catch (UnsupportedLookAndFeelException ex) {
                            System.err.println("ERROR AL INICIALIZAR UI_THEME");
                        }
                    break;
                    case "2":
                        try {
                            //UIManager.setLookAndFeel(new FlatCyanLightIJTheme());FlatLightLaf
                            UIManager.setLookAndFeel(new FlatLightLaf());
                        } catch (UnsupportedLookAndFeelException ex) {
                            System.err.println("ERROR AL INICIALIZAR UI_THEME");
                        }
                    break;
                }
            }
            
            FormHome home = new FormHome();
            ControllerPreferences controllerPreferences = new ControllerPreferences(home);
            controllerPreferences.initViewMenu();
            home.setVisible(true);
            
            ControllerTabHome controllerHome = new ControllerTabHome(home);
            controllerHome.initCardsView();
            
            ControllerTabInventario controllerTabInventory = new ControllerTabInventario(home);
            controllerTabInventory.initViewInventory();
            
            new ControllerNuevaVenta(home);
            
            ControllerTabCompras controllerTabCompras = new ControllerTabCompras(home);
            controllerTabCompras.cargarTablaProveedores();
            
        }
    }

    private String getTheme(String pathName) {
        File file = new File(pathName);
        String value;
        try {
            BufferedReader out = new BufferedReader(new FileReader(file));
            value = out.readLine();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControllerLogin.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(ControllerLogin.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return value;
    }
}
