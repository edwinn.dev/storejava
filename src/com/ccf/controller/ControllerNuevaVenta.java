package com.ccf.controller;

import com.ccf.model.Product;
import static com.ccf.util.Message.SYSTEM_MESSAGE;
import com.ccf.util.UtilTable;
import com.ccf.view.FormAddProduct;
import com.ccf.view.FormClient;
import com.ccf.view.FormHome;
import com.ccf.view.dialog.DialogFinalVenta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerNuevaVenta implements ActionListener, MouseListener {
    private FormHome formHome = null;
    private FormAddProduct formAddProduct = null;
    private DefaultTableModel dtmProductos = null;
    private List<Product> productos = null;
    private Double importeTotal = 0.0;
    private int row = -5;

    public ControllerNuevaVenta(FormHome formHome) {
        this.formHome = formHome;
        setListener();
    }

    private void setListener() {
        this.formHome.btnBuscarCliente.addActionListener(this);
        this.formHome.btnNuevoCliente.addActionListener(this);
        this.formHome.btnAddProductSale.addActionListener(this);
        this.formHome.btnDeleteProductSale.addActionListener(this);
        this.formHome.btnContinueSale.addActionListener(this);
        this.formHome.tablaProductosVenta.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == formHome.btnBuscarCliente) {

        }

        if (e.getSource() == formHome.btnNuevoCliente) {
            FormClient formCliente = new FormClient(formHome, true);
            formCliente.btnActualizarCliente.setEnabled(false);
            ControllerClient controllerClient = new ControllerClient(formCliente);
            controllerClient.iniciarVistaCliente();
        }

        if (e.getSource() == formHome.btnAddProductSale) {
            this.formAddProduct = new FormAddProduct(formHome, true);
            ControllerAddProduct controllerAddProduct = new ControllerAddProduct(formAddProduct, this);
            productos = new ArrayList<>();
            dtmProductos = (DefaultTableModel) this.formHome.tablaProductosVenta.getModel();
            controllerAddProduct.llenarTablaProductos();
            controllerAddProduct.initViewAddProduct();
            return;
        }

        if (e.getSource() == formHome.btnDeleteProductSale) {
            if (row < 0) {
                JOptionPane.showMessageDialog(formHome, "Seleccione un producto\npara eliminar.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            String idProductDelete = formHome.tablaProductosVenta.getValueAt(row, 1).toString();
            Product pDelete = null;
            for (Product p : productos) {
                if(p.getCodProducto().equals(idProductDelete)){
                    pDelete = p;
                }
            }
            int response = JOptionPane.showConfirmDialog(formHome, "¿Esta seguro de eliminar el producto\nde la lista?", SYSTEM_MESSAGE, JOptionPane.YES_NO_OPTION);
            if(response == JOptionPane.YES_OPTION){
                removeProductToList(pDelete);
            }
        }

        if (e.getSource() == formHome.btnContinueSale) {
            DialogFinalVenta dialogFinalShop = new DialogFinalVenta(formHome, true);
            dialogFinalShop.setLocationRelativeTo(formHome);
            dialogFinalShop.lblProductsQuantity.setText("Total de productos : " + productos.size());
            dialogFinalShop.txtImporteBase.setText("" + this.importeTotal);
            dialogFinalShop.setVisible(true);
        }
    }

    public void addProductsToList(Product product) {
        this.productos.add(product);
        this.importeTotal += product.getAuxSubtotal();
        UtilTable.clearTableModel(this.formHome.tablaProductosVenta);
        this.productos.forEach(p -> {
            dtmProductos.addRow(new Object[]{
                p.getIdProducto(), p.getCodProducto(), p.getNombreProducto(), p.getNombreUnMedida(), p.getAuxQuantity(), p.getAuxPrecioUnitario(), p.getAuxSubtotal()
            });
        });
        formHome.txtImporteTotal.setText(String.format("%.2f", importeTotal));
    }

    private void removeProductToList(Product product) {
        this.productos.remove(product);
        importeTotal -= product.getAuxSubtotal();
        UtilTable.clearTableModel(this.formHome.tablaProductosVenta);
        this.productos.forEach(p -> {
            dtmProductos.addRow(new Object[]{
                p.getIdProducto(), p.getCodProducto(), p.getNombreProducto(), p.getNombreUnMedida(), p.getAuxQuantity(), p.getAuxPrecioUnitario(), p.getAuxSubtotal()
            });
        });
        formHome.txtImporteTotal.setText(String.format("%.2f", importeTotal));
        JOptionPane.showMessageDialog(formHome, "Producto eliminado de\nla lista.", SYSTEM_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.row = formHome.tablaProductosVenta.getSelectedRow();
        this.formHome.btnDeleteProductSale.setEnabled(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
