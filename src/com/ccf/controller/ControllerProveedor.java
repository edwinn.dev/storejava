package com.ccf.controller;

import com.ccf.dao.ProveedorDao;
import com.ccf.model.Proveedor;
import static com.ccf.util.Message.SYSTEM_MESSAGE;
import com.ccf.view.FormProveedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControllerProveedor implements ActionListener {
    private FormProveedor formProveedor = null;
    private ProveedorDao proveedorDao = null;
    private ControllerTabCompras controllerTabCompras = null;

    public ControllerProveedor(FormProveedor formProveedor, ProveedorDao proveedorDao, ControllerTabCompras controllerTabCompras) {
        this.formProveedor = formProveedor;
        this.proveedorDao = proveedorDao;
        this.controllerTabCompras = controllerTabCompras;
        this.setListener();
    }

    private void setListener() {
        this.formProveedor.btnGuardarProveedor.addActionListener(this);
        this.formProveedor.btnActualizarProveedor.addActionListener(this);
    }

    public void iniciarVistaFormProveedor() {
        this.formProveedor.setResizable(false);
        this.formProveedor.setLocationRelativeTo(null);
        this.formProveedor.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == formProveedor.btnGuardarProveedor) {
            registrarProveedor();
        } else if (e.getSource() == formProveedor.btnActualizarProveedor) {
            actualizaProveedor();
        }
    }

    private void registrarProveedor() {
        if (formProveedor.txtRazonSocial.getText().isEmpty() || formProveedor.txtRuc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(formProveedor, "La Razon Social y RUC son requeridos\npara registrar al proveedor.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
            return;
        }

        Proveedor proveedor = new Proveedor();
        proveedor.setRazonSocial(formProveedor.txtRazonSocial.getText().toUpperCase().trim());
        proveedor.setRuc(formProveedor.txtRuc.getText().toUpperCase().trim());
        proveedor.setDireccion(formProveedor.txtDireccion.getText().isEmpty() ? "-" : formProveedor.txtDireccion.getText().toUpperCase().trim());
        proveedor.setEmail(formProveedor.txtEmail.getText().isEmpty() ? "-" : formProveedor.txtEmail.getText().toLowerCase().trim());
        proveedor.setTelefono(formProveedor.txtTelefono.getText().isEmpty() ? "-" : formProveedor.txtTelefono.getText().trim());

        int respuesta = proveedorDao.create(proveedor);
        if (respuesta <= 0) {
            JOptionPane.showMessageDialog(formProveedor, "Error al registrar proveedor\nverique si los datos son correctos.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
            formProveedor.dispose();
            return;
        }
        formProveedor.dispose();
        controllerTabCompras.cargarTablaProveedores();
        JOptionPane.showMessageDialog(formProveedor, "Proveedor registrado.", SYSTEM_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
    }

    private void actualizaProveedor() {
        if (formProveedor.txtRazonSocial.getText().isEmpty() || formProveedor.txtRuc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(formProveedor, "La Razon Social y RUC son requeridos\npara actualizar al proveedor.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
            return;
        }

        Proveedor proveedor = new Proveedor();
        proveedor.setRazonSocial(formProveedor.txtRazonSocial.getText().toUpperCase().trim());
        proveedor.setRuc(formProveedor.txtRuc.getText().toUpperCase().trim());
        proveedor.setDireccion(formProveedor.txtDireccion.getText().isEmpty() ? "-" : formProveedor.txtDireccion.getText().toUpperCase().trim());
        proveedor.setEmail(formProveedor.txtEmail.getText().isEmpty() ? "-" : formProveedor.txtEmail.getText().toLowerCase().trim());
        proveedor.setTelefono(formProveedor.txtTelefono.getText().isEmpty() ? "-" : formProveedor.txtTelefono.getText().trim());

        int respuesta = proveedorDao.update(proveedor);
        if (respuesta <= 0) {
            JOptionPane.showMessageDialog(formProveedor, "Error al actualizar proveedor\nintente nuevamente", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
            formProveedor.dispose();
            return;
        }
        formProveedor.dispose();
        controllerTabCompras.cargarTablaProveedores();
        JOptionPane.showMessageDialog(formProveedor, "Proveedor actualizado.", SYSTEM_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
    }
}
