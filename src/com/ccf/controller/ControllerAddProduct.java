package com.ccf.controller;

import static com.ccf.util.Message.SYSTEM_MESSAGE;
import com.ccf.dao.ProductoDao;
import com.ccf.model.Product;
import com.ccf.util.UtilTable;
import com.ccf.view.FormAddProduct;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerAddProduct implements ActionListener{
    private FormAddProduct formAddProduct = null;
    private ControllerNuevaVenta controllerNewSale = null;
    private ProductoDao productoDao = null;
    
    public ControllerAddProduct(FormAddProduct formAddProduct, ControllerNuevaVenta controllerNewSale){
        this.formAddProduct = formAddProduct;
        this.controllerNewSale = controllerNewSale;
        this.productoDao = new ProductoDao();
        setListener();
    }
    
    private void setListener(){
        this.formAddProduct.btnAddProduct.addActionListener(this);
    }
    
    public void initViewAddProduct(){
        this.formAddProduct.setLocationRelativeTo(null);
        this.formAddProduct.setResizable(false);
        this.formAddProduct.setVisible(true);
    }
    
    public void llenarTablaProductos(){
        DefaultTableModel model = (DefaultTableModel)this.formAddProduct.tablaProductos.getModel();
        List<Product> productos = productoDao.findAll();
        UtilTable.clearTableModel(this.formAddProduct.tablaProductos);
        productos.forEach(p ->{
            model.addRow(new Object[]{
                p.getIdProducto(), p.getCodProducto(), p.getNombreProducto(), p.getPrecioVenta(), p.getNombreUnMedida(), p.getStock(), p.getNombreCategoria()
            });
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == formAddProduct.btnAddProduct){
            if(formAddProduct.txtIdProducto.getText().isEmpty()){
                JOptionPane.showMessageDialog(formAddProduct, "Seleecione un producto\npara continuar.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(formAddProduct.txtCantidad.getText().isEmpty()){
                JOptionPane.showMessageDialog(formAddProduct, "La cantidad es requerida.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            Double cantidad = Double.parseDouble(formAddProduct.txtCantidad.getText().trim());
            Double pUnitario = Double.parseDouble(formAddProduct.txtPrecioUnitario.getText().trim());
            Product p = new Product();
            p.setIdProducto(Long.parseLong(formAddProduct.txtIdProducto.getText().trim()));
            p.setCodProducto(formAddProduct.txtCodigo.getText().trim());
            p.setNombreProducto(formAddProduct.txtDescripcion.getText().trim());
            p.setNombreUnMedida(formAddProduct.txtUnidadMedida.getText().trim());
            p.setAuxQuantity(cantidad);
            p.setAuxPrecioUnitario(pUnitario);
            p.setAuxSubtotal(pUnitario * cantidad);
            controllerNewSale.addProductsToList(p);
            formAddProduct.txtCantidad.setText("");
        }
    }
}
