package com.ccf.controller;

import com.ccf.model.Cliente;
import com.ccf.util.Http;
import static com.ccf.util.Message.SYSTEM_MESSAGE;
import com.ccf.view.FormClient;
import com.google.gson.Gson;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ControllerClient implements ActionListener{
    private FormClient formCliente = null;
    private Gson gson = null;
    
    public ControllerClient(FormClient formCliente){
        this.formCliente = formCliente;
        setListener();
        this.gson = new Gson();
    }
    
    private void setListener(){
        this.formCliente.btnBuscar.addActionListener(this);
    }
    
    public void iniciarVistaCliente(){
        this.formCliente.setTitle("Registrar cliente");
        this.formCliente.setLocationRelativeTo(null);
        this.formCliente.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == formCliente.btnBuscar){
            try {
                String value = Http.httpGetDniClient(formCliente.txtNroDoc.getText().trim());
                System.out.println("value = " + value);
                Cliente cliente = gson.fromJson(value, Cliente.class);
                this.formCliente.txtNombre.setText(cliente.getNombre());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(formCliente, "El DNI ingresado no existe", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(ControllerClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
