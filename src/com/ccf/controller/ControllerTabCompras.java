package com.ccf.controller;

import static com.ccf.util.Message.SYSTEM_MESSAGE;
import com.ccf.dao.ProveedorDao;
import com.ccf.model.Proveedor;
import com.ccf.util.UtilTable;
import com.ccf.view.FormHome;
import com.ccf.view.FormProveedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerTabCompras implements ActionListener, MouseListener{
    private FormHome formHome = null;
    private ProveedorDao proveedorDao = null;
    private int row = -5;
    
    public ControllerTabCompras(FormHome formHome){
        this.formHome = formHome;
        this.proveedorDao = new ProveedorDao();
        this.setListener();
    }
    
    private void setListener(){
        this.formHome.tablaProveedores.addMouseListener(this);
        this.formHome.btnNuevoProveedor.addActionListener(this);
        this.formHome.btnModificarProveedor.addActionListener(this);
        this.formHome.btnEliminarProveedor.addActionListener(this);
    }
    
    public void cargarTablaProveedores(){
        DefaultTableModel dtmProveedor = (DefaultTableModel)formHome.tablaProveedores.getModel();
        UtilTable.clearTableModel(formHome.tablaProveedores);
        List<Proveedor> proveedores = proveedorDao.findAll();
        proveedores.forEach(p ->{
            dtmProveedor.addRow(new Object[]{
                p.getIdProveedor(), p.getRazonSocial(), p.getRuc(), p.getDireccion(), p.getTelefono(), p.getEmail()
            });
        });
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == formHome.btnNuevoProveedor){
            FormProveedor formProveedor = new FormProveedor(formHome, true);
            ControllerProveedor controllerProveedor = new ControllerProveedor(formProveedor, proveedorDao, this);
            formProveedor.btnActualizarProveedor.setEnabled(false);
            controllerProveedor.iniciarVistaFormProveedor();
        }else if(e.getSource() == formHome.btnModificarProveedor){
            FormProveedor formProveedor = new FormProveedor(formHome, true);
            ControllerProveedor controllerProveedor = new ControllerProveedor(formProveedor, proveedorDao, this);
            formProveedor.setTitle("Modificar Proveedor");
            formProveedor.btnGuardarProveedor.setEnabled(false);
            controllerProveedor.iniciarVistaFormProveedor();
        }else if(e.getSource() == formHome.btnEliminarProveedor){
            if(row < 0){
                JOptionPane.showMessageDialog(formHome, "Seleccion un registro para continuar.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            Long idProveedor = Long.parseLong(formHome.tablaProveedores.getValueAt(row, 0).toString());
            int respuesta = proveedorDao.delete(new Proveedor(idProveedor));
            if(respuesta <= 0){
                JOptionPane.showMessageDialog(formHome, "Error al eliminar al proveedor.\nIntente nuevamente.", SYSTEM_MESSAGE, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(formHome, "Proveedor eliminado.", SYSTEM_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
            this.cargarTablaProveedores();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.row = formHome.tablaProveedores.getSelectedRow();
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
